$(document).ready(function () {
var screen_ht = $(window).height();
	var preloader_ht = 5;
	var padding =(screen_ht/2)-preloader_ht;
	
	$("#preloader").css("padding-top",padding+"px");
    $("#select1").click(function () {
        $("#1").stop();
        $("#2").stop();
        $("#3").stop();
        $("#4").stop();
        $("#1").show();
        $("#2").hide();
        $("#3").hide();
        $("#4").hide();
        $("#myCarousel").carousel(0);
        $("#myCarousel").carousel('pause');
    });
    $("#select2").click(function () {
        $("#1").stop();
        $("#2").stop();
        $("#3").stop();
        $("#4").stop();
        $("#1").hide();
        $("#2").show();
        $("#3").hide();
        $("#4").hide();
        $("#myCarousel2").carousel(0);
        $("#myCarousel2").carousel('pause');
    });
    $("#select3").click(function () {
        $("#1").stop();
        $("#2").stop();
        $("#3").stop();
        $("#4").stop();
        $("#1").hide();
        $("#2").hide();
        $("#3").show();
        $("#4").hide();
        $("#myCarousel3").carousel(0);
        $("#myCarousel3").carousel('pause');
    });
    $("#select4").click(function () {
        $("#1").stop();
        $("#2").stop();
        $("#3").stop();
        $("#4").stop();
        $("#1").hide();
        $("#2").hide();
        $("#3").hide();
        $("#4").show();
        $("#myCarousel4").carousel(0);
        $("#myCarousel4").carousel('pause');
    });

    function sample() {
        var y = $(window).scrollTop(); //your current y position on the page
        $(window).scrollTop(y - 150);
    }
    $("#s1i1").click(function () {
        $("#myCarousel").carousel(0);
        $("#myCarousel").carousel('pause');
        $('.card').removeClass("copa");
        $(this).parent().addClass("copa");
        setTimeout(sample, 200);
    });
    $("#s1i2").click(function () {
        $("#myCarousel").carousel(1);
        $("#myCarousel").carousel('pause');
        $('.card').removeClass("copa");
        $(this).parent().addClass("copa");
        setTimeout(sample, 200);
    });
    $("#s1i3").click(function () {
        $("#myCarousel").carousel(2);
        $("#myCarousel").carousel('pause');
        $('.card').removeClass("copa");
        $(this).parent().addClass("copa");
        setTimeout(sample, 200);
    });
    $("#s1i4").click(function () {
        $("#myCarousel").carousel(3);
        $("#myCarousel").carousel('pause');
        $('.card').removeClass("copa");
        $(this).parent().addClass("copa");
        setTimeout(sample, 200);
    });

    $("#s2i1").click(function () {
        $("#myCarousel2").carousel(0);
        $("#myCarousel2").carousel('pause');
        $('.card').removeClass("copa");
        $(this).parent().addClass("copa");
        setTimeout(sample, 200);
    });
    $("#s2i2").click(function () {
        $("#myCarousel2").carousel(1);
        $("#myCarousel2").carousel('pause');
        $('.card').removeClass("copa");
        $(this).parent().addClass("copa");
        setTimeout(sample, 200);
    });
    $("#s2i3").click(function () {
        $("#myCarousel2").carousel(2);
        $("#myCarousel2").carousel('pause');
        $('.card').removeClass("copa");
        $(this).parent().addClass("copa");
        setTimeout(sample, 200);
    });

    $("#s3i1").click(function () {
        $("#myCarousel3").carousel(0);
        $("#myCarousel3").carousel('pause');
        $('.card').removeClass("copa");
        $(this).parent().addClass("copa");
        setTimeout(sample, 200);
    });
    $("#s3i2").click(function () {
        $("#myCarousel3").carousel(1);
        $("#myCarousel3").carousel('pause');
        $('.card').removeClass("copa");
        $(this).parent().addClass("copa");
        setTimeout(sample, 200);
    });
    $("#s3i3").click(function () {
        $("#myCarousel3").carousel(2);
        $("#myCarousel3").carousel('pause');
        $('.card').removeClass("copa");
        $(this).parent().addClass("copa");
        setTimeout(sample, 200);
    });

    $("#s4i1").click(function () {
        $("#myCarousel4").carousel(0);
        $("#myCarousel4").carousel('pause');
        $('.card').removeClass("copa");
        $(this).parent().addClass("copa");
        setTimeout(sample, 200);
    });
    $("#s4i2").click(function () {
        $("#myCarousel4").carousel(1);
        $("#myCarousel4").carousel('pause');
        $('.card').removeClass("copa");
        $(this).parent().addClass("copa");
        setTimeout(sample, 200);
    });
    $("#s4i3").click(function () {
        $("#myCarousel4").carousel(2);
        $("#myCarousel4").carousel('pause');
        $('.card').removeClass("copa");
        $(this).parent().addClass("copa");
        setTimeout(sample, 200);
    });
    $('.card').hover(function () {
        $(this).find('img').fadeTo(500, 0.5);
    }, function () {
        $(this).find('img').fadeTo(500, 1);
    });
});
