// including plugins
var gulp = require('gulp');
var nunjucksRender = require('gulp-nunjucks-render');
var browserSync = require('browser-sync').create();
var data = require('gulp-data');
var prettify = require('gulp-html-prettify');
 
gulp.task('beautify', function() {
  gulp.src('./app/*.html')
    .pipe(prettify({indent_char: ' ', indent_size: 2}))
    .pipe(gulp.dest('./app/'))
});

// Static server
gulp.task('default', ['compile'], function () {
    browserSync.init({
        server: {
            baseDir: "./app"
        }
    });
    gulp.watch("templates/**/*.html", ['compile']);
    gulp.watch("templates/**/*.nunjucks", ['compile']);
    gulp.watch("app/*.html").on('change', browserSync.reload);
});


gulp.task('compile', function () {
    // Gets .html and .nunjucks files in pages
    return gulp.src('templates/pages/**/*.+(html|nunjucks)')
        // Renders template with nunjucks
        .pipe(data(function () {
            return require('./templates/data.json')
        }))
        .pipe(nunjucksRender({
            path: ['templates/components']
        }))
        // output files in app folder
        .pipe(gulp.dest('app'))
});

 
